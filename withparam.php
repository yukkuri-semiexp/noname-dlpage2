<?php

class WithParam {
    
    public function __construct($obj,$param){
        $this->obj=$obj;
        $this->param=$param;
    }
    
    public $obj,$param;
    
}