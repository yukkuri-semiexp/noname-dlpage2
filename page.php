<?php

require_once "withparam.php";

class Page{
    
    private static $pages=array();
    
    public function __construct(string $title=null){
        $this->title=$title;
    }
    
    public $widgets=array();
    
    public $title;
    
    public function getTitle(){
        return $this->title;
    }
    
    public function addWidget($widget,array $param){
        array_push($this->widgets,new WithParam($widget,$param));
    }
    
    public function dispPage(){
        foreach($this->widgets as $widget){
            $widget->obj->dispWidget($widget->param,null);
            print("<br>");
        }
    }
    
    public static function registPage(string $id,$page){
        Page::$pages[$id]=$page;
    }
    
    public static function getPage(string $id){
        return Page::$pages[$id];
    }
    
    public static function getPages(){
        return Page::$pages;
    }
    
}
?>
