<?php

require_once "widget.php";
require_once "license.php";
require_once "withparam.php";

class Download_File implements Widget{
    
    public function __construct(string $url,string $license,string $name,string $version,string $type=null,string $author=null,int $year=null,string $license_url="#"){
        $this->url=$url;
        $this->license=$license;
        $this->name=$name;
        $this->version=$version;
        $this->type=$type;
        $this->author=$author;
        $this->year=$year;
        $this->license_url=$license_url;
    }
    
    public function __clone(){
        $newobj=new Download_File($this->url,$this->license,$this->name,$this->version,$this->type,$this->author,$this->year,$this->license_url);
    }
    
    public $url,$license,$name,$version,$type,$author,$year,$license_url;
    
    private $widgets=array();
    
    public function addWidget($widget,array $param){
        $widget->onAdd();
        array_push($this->widgets,new WithParam($widget,$param));
    }
    
    public function widgets(){
        return $this->widgets;
    }
    
    public function onAdd(){}
    
    public function dispWidget(array $param,$w_envc){
        if($this->type!=null){
            $typedisp="(".$this->type.")";
        }else{
            $typedisp="";
        }
        print("<h2>".htmlspecialchars($this->name)." ".$this->version.$typedisp."</h2>");
        foreach($this->widgets as $widget){
            $widget->obj->dispWidget($widget->param,$this);
            print("<br>");
        }
    }
    
    public function require_components(){}
    
}



