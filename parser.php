<?php

require_once "plugin.php";
require_once "widget.php";
require_once "license.php";
require_once "page.php";
require_once "download_File.php";

function parse(string $CONFIGURE_XML){
    $SCHEMA="https://c19092ff.ddns.net/downloads/data/downloads.xsd";
	$dom=new DOMDocument();
	$dom->load($CONFIGURE_XML);
	if(!($dom->schemaValidate($SCHEMA))){
	    print("Invalid XML");
		return false;
    }
    $root=$dom->getElementsByTagName("download-configuration")->item(0);
    foreach($root->getElementsByTagName("include") as $include_f){
        parse($include_f->textContent);
    }
    foreach($root->getElementsByTagName("plugin") as $plugin){
        $plugin_id=$plugin->getAttribute("name");
        $plugin_params=array();
        foreach($plugin->getElementsByTagName("parameter") as $param){
            $plugin_params[$param->getAttribute("name")]=$param->getAttribute("value");
        }
        $plugin_instance=require("data/plug-ins/".$plugin_id."/main.php");
        if($plugin_instance instanceof Plugin){
            $plugin_instance->onLoad($plugin_params);
        }
    }
    foreach($root->getElementsByTagName("page") as $page){
        $page_name=$page->getAttribute("name");
        $page_title=$page->getAttribute("title");
        $page_i=new Page($page_title);
        if(!isset($_GET['page'])||$_GET['page']==$page_name){
            Page::registPage($page_name,$page_i);
        }
        foreach($page->childNodes as $widget){
            switch($widget->nodeName){
            case "dl-conf:line":
                $page_i->addWidget(new InlineHTML("<hr>"),array());
                break;
            case "dl-conf:html":
                $page_i->addWidget(new InlineHTML($widget->textContent),array());
                break;
            case "dl-conf:view":
                $params=array();
                foreach($widget->getElementsByTagName("parameter") as $param){
                    $params[$param->getAttribute("name")]=$param->getAttribute("value");
                }
                WidgetRegistry::getWidget($widget->getAttribute("type"))->onAdd();
                $page_i->addWidget(WidgetRegistry::getWidget($widget->getAttribute("type")),$params);
                break;
            case "dl-conf:download-file":
                $params=array();
                foreach($widget->getElementsByTagName("parameter") as $param){
                    $params[$param->getAttribute("name")]=$param->getAttribute("value");
                }
                $dlf_i=new Download_File($widget->getAttribute("url"),$widget->getAttribute("license"),$widget->getAttribute("name"),$widget->getAttribute("version"),$widget->getAttribute("type"),$widget->getAttribute("author"),intval($widget->getAttribute("release-year")),$widget->getAttribute("license-url"));
                foreach($widget->getElementsByTagName("view") as $dlfv){
                    $params_dlfv=array();
                    foreach($dlfv->getElementsByTagName("parameter") as $param){
                        $params_dlfv[$param->getAttribute("name")]=$param->getAttribute("value");
                    }
                    $dlf_i->addWidget(WidgetRegistry::getWidget($dlfv->getAttribute("type")),$params_dlfv);
                }
                $page_i->addWidget($dlf_i,$params);
                break;
            }
        }
        Page::registPage($page_name,$page_i);
    }
    {
        $licenses=$root->getElementsByTagName("licenses")[0];
        if($licenses!=null){
            foreach($licenses->getElementsByTagName("permission") as $permission){
                $permission_id=$permission->getAttribute("name");
                $permission_label=$permission->getAttribute("label");
                $permission_i=new Permission($permission_label);
                foreach($permission->getElementsByTagName("description") as $desc){
                    $desc_type=intval($desc->getAttribute("type"));
                    $desc_text=$desc->getAttribute("text");
                    $permission_i->setDescription($desc_type,$desc_text);
                }
                Permission::registPermission($permission_id,$permission_i);
            }
            foreach($licenses->getElementsByTagName("license") as $license){
                $license_id=$license->getAttribute("name");
                $license_i=new License($license->getAttribute("label"),$license->getAttribute("icon"));
                foreach($license->childNodes as $permission){
                    if(!($permission instanceof DOMElement)){
                        continue;
                    }
                    $permission_id=$permission->getAttribute("name");
                    switch($permission->tagName){
                        case 'dl-conf:permitted':
                            $license_i->addPermitted($permission_id);
                            break;
                        case 'dl-conf:condition':
                            $license_i->addCondition($permission_id);
                            break;
                        case 'dl-conf:limited':
                            $license_i->addLimited($permission_id);
                            break;
                        case 'dl-conf:prohibited':
                            $license_i->addProhibited($permission_id);
                            break;
                    }
                }
                License::registLicense($license_id,$license_i);
            }
        }
    }
}
?>
