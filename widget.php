<?php

interface Widget
{
    
    public function onAdd();
    
    public function dispWidget(array $param,$w_env);
    
    public function require_components();
    
}

class InlineHTML implements Widget{
    
    public function __construct(string $html){
        $this->html=$html;
    }
    
    private $html;
    
    public function onAdd(){}
    
    public function dispWidget(array $param,$w_env){
        print($this->html);
    }
    
    public function require_components(){
        return array();
    }
    
}

class WidgetRegistry{
    
    private function __construct(){}
    
    private static $widgets=array();
    
    private static $used_widgets=array();
    
    public static function registerWidget(string $id,$widget){
        WidgetRegistry::$widgets[$id]=$widget;
    }
    
    public static function getWidget(string $id){
        return WidgetRegistry::$widgets[$id];
    }
    
}

function checkWidgetRequire($widget,array $available_component){
    foreach($widget->require_components() as $component){
        if(!in_array($component,$available_component)){
            return false;
        }
    }
    return true;
}