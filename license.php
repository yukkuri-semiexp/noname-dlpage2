<?php

define("PERMITTED",0x1);
define("CONDITION",0x2);
define("RESTRICTED",0x4);
define("PROHIBITED",0x8);

class License
{
    
    private static $license_map=array();
    
    public function __construct(string $label=null,string $icon=null){
        $this->label=$label;
        $this->icon=$icon;
    }
    
    public $label,$icon;
    
    private $permitted=array();#「許可」表示
    
    private $condition=array();#「条件」表示
    
    private $limited=array();#「制限」表示
    
    private $prohibited=array();#「禁止」表示
    
    public function addPermitted(string $permissionID){
        array_push($this->permitted,$permissionID);
    }
    
    public function getPermitted(){
        return $this->permitted;
    }
    
    public function addCondition(string $permissionID){
        array_push($this->condition,$permissionID);
    }
    
    public function getCondition(){
        return $this->condition;
    }
    
    public function addLimited(string $permissionID){
        array_push($this->limited,$permissionID);
    }
    
    public function getLimited(){
        return $this->limited;
    }
    
    public function addProhibited(string $permissionID){
        array_push($this->prohibited,$permissionID);
    }
    
    public function getProhibited(){
        return $this->prohibited;
    }
    
    public static function registLicense(string $id,License $license){
        License::$license_map[$id]=$license;
    }
    
    public static function getLicense(string $id){
        return License::$license_map[$id];
    }
    
}

class Permission {
    
    private static $permission_map=array();
    
    public function __construct(string $label) {
        $this->label=$label;
    }
    
    private $label,$description=array();
    
    public function getLabel(){
        return $this->label;
    }
    
    public function getDescription(int $type){
        if(isset($this->description[$type])){
            return $this->description[$type];
        }else{
            for($i=0x1;$i<=0x1+0x2+0x4+0x8;$i++){
                if(($i&$type)!=0&&isset($this->description[$type])){
                    return $this->description[$type];
                }
            }
        }
    }
    
    public function setDescription(int $type,string $description){
        $this->description[$type]=$description;
    }
    
    public static function registPermission(string $id,$permission){
        Permission::$permission_map[$id]=$permission;
    }
    
    public static function getPermission(string $id){
        return Permission::$permission_map[$id];
    }
    
}