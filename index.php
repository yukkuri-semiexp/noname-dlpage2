<?php
require_once "page.php";
require_once "parser.php";
require_once "css.php";

parse("data/downloads.xml");

if(isset($_GET['page'])){
    $page=Page::getPage($_GET['page']);
}
?>
<html>
	<head>
		<title><?php 
		if(isset($page)){
		    print($page->title);
		}else{
		    print("ダウンロード");
		}
		?></title>
		<?php CSS::printTag();?>
	</head>
	<body><?php 
	if(isset($page)){
	    $page->dispPage();
	}else{
	    print("<h1>ダウンロードページの一覧</h1>");
	    print("<ul>");
	    foreach(Page::getPages() as $name => $page){
	        print("<li><a href=\"?page=".$name."\">".$page->title."</a>");
	    }
	    print("</ul>");
	}
	?></body>
</html>