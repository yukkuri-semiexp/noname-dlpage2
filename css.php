<?php

class CSS
{
    
    private static $css_list=array();
    
    public static function readCSS(string $css){
        if(in_array($css,CSS::$css_list)){
            return;
        }
        array_push(CSS::$css_list,$css);
    }
    
    public static function printTag(){
        foreach(CSS::$css_list as $css){
            print("<link rel=\"stylesheet\" href=\"".$css."\">");
        }
    }
    
}

